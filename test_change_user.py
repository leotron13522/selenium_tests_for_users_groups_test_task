import requests

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.support.ui import Select


def test_change_user(browser, groups_and_users):
    users_before_test = requests.get('http://127.0.0.1:8000/users/').json()

    # add valid user
    browser.get('http://127.0.0.1/users/')

    edit_button = browser.find_element_by_xpath('//tr[td[text()="user-1"]]//td/a/button')
    edit_button.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()
    form_name.send_keys('test-user-21')

    select_group = Select(browser.find_element_by_tag_name('select'))
    select_group.select_by_visible_text('group-2')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'User updated.', f'expected "User updated.", got {alert.text}'
    alert.accept()

    browser.get('http://127.0.0.1/users/')
    changed_user_id = browser.find_element_by_xpath('//tr[td[text()="test-user-21"]]').text.split()[0]
    changed_user = requests.get('http://127.0.0.1:8000/users/{}'.format(changed_user_id)).json()
    assert changed_user['name'] == 'test-user-21'
    expected_group_name = requests.get('http://127.0.0.1:8000/groups/{}'.format(changed_user['group'])).json()[
        'name']
    assert expected_group_name == 'group-2'

    users_after_valid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert len(users_before_test) == len(users_after_valid_test)

    # change user with empty name

    users_before_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()

    browser.get('http://127.0.0.1/users/')

    edit_button = browser.find_element_by_xpath('//tr[td[text()="user-2"]]//td/a/button')
    edit_button.click()

    # leave name-field empty
    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()

    select_group = Select(browser.find_element_by_tag_name('select'))
    select_group.select_by_visible_text('group-2')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s name-form did not call mistake", got {alert.text}'
    alert.accept()

    users_after_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert users_before_invalid_test == users_after_invalid_test

    # change user with empty group select-form

    users_before_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()

    browser.get('http://127.0.0.1/users/')

    edit_button = browser.find_element_by_xpath('//tr[td[text()="user-2"]]//td/a/button')
    edit_button.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()
    form_name.send_keys('test-user-1233')

    # choose group select-field empty
    select_group = Select(browser.find_element_by_tag_name('select'))
    select_group.select_by_visible_text('')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s description-form did not call mistake", got {alert.text}'
    alert.accept()

    users_after_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert users_before_invalid_test == users_after_invalid_test
