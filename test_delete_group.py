import requests

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_add_group(browser, groups_and_users):
    groups_before_test = requests.get('http://127.0.0.1:8000/groups/').json()

    # add valid group
    browser.get('http://127.0.0.1/groups/')
    button_groups = browser.find_element_by_link_text('Groups')
    button_groups.click()

    button_add_groups = browser.find_element_by_class_name('add-btn')
    button_add_groups.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.send_keys('test-group-17')

    form_description = browser.find_element_by_tag_name('textarea')
    form_description.send_keys('test-description-17')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'Group created.', f'expected "Group created.", got {alert.text}'
    alert.accept()

    last_added_group = requests.get('http://127.0.0.1:8000/groups/').json()[-1]
    assert last_added_group['name'] == 'test-group-17'
    assert last_added_group['description'] == 'test-description-17'

    browser.get('http://127.0.0.1/groups/')
    delete_button = browser.find_element_by_xpath('//tr[td[text()="test-group-17"]]//td/button')
    delete_button.click()

    groups_after_valid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_test == groups_after_valid_test

    # try to delete group with user

    delete_button = browser.find_element_by_xpath('//tr[td[text()="test-group-1"]]//td/button')
    delete_button.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'Can\'t delete group!', f'expected "Can\'t delete group!", got {alert.text}'
    alert.accept()

    groups_after_valid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_test == groups_after_valid_test
