import requests

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_add_group(browser, groups_and_users):
    groups_before_test = requests.get('http://127.0.0.1:8000/groups/').json()

    # add valid group
    browser.get('http://127.0.0.1/groups/')
    button_groups = browser.find_element_by_link_text('Groups')
    button_groups.click()

    button_add_groups = browser.find_element_by_class_name('add-btn')
    button_add_groups.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.send_keys('test-group-1')

    form_description = browser.find_element_by_tag_name('textarea')
    form_description.send_keys('test-description-1')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'Group created.', f'expected "Group created.", got {alert.text}'
    alert.accept()

    last_added_group = requests.get('http://127.0.0.1:8000/groups/').json()[-1]
    assert last_added_group['name'] == 'test-group-1'
    assert last_added_group['description'] == 'test-description-1'
    groups_and_users['group'].append(last_added_group['id'])

    groups_after_valid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert len(groups_before_test) == len(groups_after_valid_test) - 1

    # add invalid group with empty name

    groups_before_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()

    browser.get('http://127.0.0.1/groups/')
    button_groups = browser.find_element_by_link_text('Groups')
    button_groups.click()

    button_add_groups = browser.find_element_by_class_name('add-btn')
    button_add_groups.click()

    # leave name-field empty
    form_description = browser.find_element_by_tag_name('textarea')
    form_description.send_keys('test-description-1')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s name-form did not call mistake", got {alert.text}'
    alert.accept()

    groups_after_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_invalid_test == groups_after_invalid_test

    # add invalid group with empty description

    groups_before_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()

    browser.get('http://127.0.0.1/groups/')
    button_groups = browser.find_element_by_link_text('Groups')
    button_groups.click()

    button_add_groups = browser.find_element_by_class_name('add-btn')
    button_add_groups.click()

    # leave description-field empty
    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.send_keys('test-group-1')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s description-form did not call mistake", got {alert.text}'
    alert.accept()

    groups_after_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_invalid_test == groups_after_invalid_test
