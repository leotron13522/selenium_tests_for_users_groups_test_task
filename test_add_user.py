import requests

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.support.ui import Select


def test_add_user(browser, groups_and_users):
    users_before_test = requests.get('http://127.0.0.1:8000/users/').json()

    # add valid user
    browser.get('http://127.0.0.1/users/')
    button_users = browser.find_element_by_link_text('Users')
    button_users.click()

    button_add_users = browser.find_element_by_class_name('add-btn')
    button_add_users.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.send_keys('test-user-1')

    select_group = Select(browser.find_element_by_tag_name('select'))
    select_group.select_by_visible_text('group-1')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'User created.', f'expected "User created.", got {alert.text}'
    alert.accept()

    last_added_user = requests.get('http://127.0.0.1:8000/users/').json()[-1]
    assert last_added_user['name'] == 'test-user-1'

    expected_group_name = requests.get('http://127.0.0.1:8000/groups/{}'.format(last_added_user['group'])).json()['name']
    assert expected_group_name == 'group-1'
    groups_and_users['user'].append(last_added_user['id'])

    users_after_valid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert len(users_before_test) == len(users_after_valid_test) - 1

    # add invalid user with empty name

    users_before_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()

    browser.get('http://127.0.0.1/users/')

    button_users = browser.find_element_by_link_text('Users')
    button_users.click()

    button_add_users = browser.find_element_by_class_name('add-btn')
    button_add_users.click()

    # leave name-field empty
    select_group = Select(browser.find_element_by_tag_name('select'))
    select_group.select_by_visible_text('group-1')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty user`s name-form did not call mistake", got {alert.text}'
    alert.accept()

    users_after_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert users_before_invalid_test == users_after_invalid_test

    # add invalid user without selecting group

    users_before_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()

    browser.get('http://127.0.0.1/users/')

    button_users = browser.find_element_by_link_text('Users')
    button_users.click()

    button_add_users = browser.find_element_by_class_name('add-btn')
    button_add_users.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.send_keys('test-user-1')
    # leave group select-field empty

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s description-form did not call mistake", got {alert.text}'
    alert.accept()

    users_after_invalid_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert users_before_invalid_test == users_after_invalid_test
