import pytest
from selenium import webdriver
import requests


@pytest.fixture
def browser():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--remote-debugging-port=9222")

    browser = webdriver.Chrome(options=chrome_options)
    yield browser
    browser.quit()


@pytest.fixture
def groups_and_users():
    groups_before_test = requests.get('http://127.0.0.1:8000/groups/').json()
    users_before_test = requests.get('http://127.0.0.1:8000/users/').json()
    id_users_to_del = []
    id_groups_to_del = []
    for j in range(1, 4):
        group_name = 'group-{}'.format(j)
        group_description = 'test-group-{}'.format(j)
        groups_url = 'http://127.0.0.1:8000/groups/'

        group_response = requests.post(groups_url, {'name': group_name,
                                                    'description': group_description})
        id_groups_to_del.append(group_response.json()['id'])

    for j in range(1, 3):
        user_name = 'user-{}'.format(j)
        users_url = 'http://127.0.0.1:8000/users/'

        user_response = requests.post(users_url, {'name': user_name,
                                                  'group': id_groups_to_del[j - 1]})
        id_users_to_del.append(user_response.json()['id'])

    id_to_del = {'group': id_groups_to_del,
                 'user': id_users_to_del}
    yield id_to_del

    for user_id in id_to_del['user']:
        requests.delete('http://127.0.0.1:8000/users/{}'.format(user_id))

    for group_id in id_to_del['group']:
        requests.delete('http://127.0.0.1:8000/groups/{}'.format(group_id))

    groups_after_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_test == groups_after_test, 'Not all test-created groups were deleted'

    users_after_test = requests.get('http://127.0.0.1:8000/users/').json()
    assert users_before_test == users_after_test, 'Not all test-created users were deleted'
