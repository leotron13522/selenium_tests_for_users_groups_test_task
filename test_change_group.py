import requests

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_change_group(browser, groups_and_users):
    groups_before_test = requests.get('http://127.0.0.1:8000/groups/').json()

    browser.get('http://127.0.0.1/groups/')

    # valid group update
    edit_button = browser.find_element_by_xpath('//tr[td[text()="group-1"]]//td/a/button')
    edit_button.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()
    form_name.send_keys('test-group-8')

    form_description = browser.find_element_by_tag_name('textarea')
    form_description.clear()
    form_description.send_keys('test-description-8')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'Group updated.', f'expected "Group updated.", got {alert.text}'
    alert.accept()

    browser.get('http://127.0.0.1/groups/')
    changed_group_id = browser.find_element_by_xpath('//tr[td[text()="test-group-8"]]').text.split()[0]
    changed_group = requests.get('http://127.0.0.1:8000/groups/{}'.format(changed_group_id)).json()
    assert changed_group['name'] == 'test-group-8'
    assert changed_group['description'] == 'test-description-8'

    groups_after_valid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert len(groups_before_test) == len(groups_after_valid_test)

    # changed group with empty name

    groups_before_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()

    browser.get('http://127.0.0.1/groups/')
    edit_button = browser.find_element_by_xpath('//tr[td[text()="group-2"]]//td/a/button')
    edit_button.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()
    # leave name-field empty

    form_description = browser.find_element_by_tag_name('textarea')
    form_description.clear()
    form_description.send_keys('test-description-8')

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s name-form did not call mistake", got {alert.text}'
    alert.accept()

    groups_after_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_invalid_test == groups_after_invalid_test

    # change group with empty description

    groups_before_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()

    browser.get('http://127.0.0.1/groups/')
    edit_button = browser.find_element_by_xpath('//tr[td[text()="group-2"]]//td/a/button')
    edit_button.click()

    form_name = browser.find_element_by_css_selector('input.form-control')
    form_name.clear()
    form_name.send_keys('test-group-8')

    form_description = browser.find_element_by_tag_name('textarea')
    form_description.clear()
    # leave description-field empty

    button_submit = browser.find_element_by_css_selector('input.btn')
    button_submit.click()

    alert = WebDriverWait(browser, 2).until(EC.alert_is_present())
    assert alert.text == 'There was an error! Please re-check your form.', \
        f'expected "Empty group`s description-form did not call mistake", got {alert.text}'
    alert.accept()

    groups_after_invalid_test = requests.get('http://127.0.0.1:8000/groups/').json()
    assert groups_before_invalid_test == groups_after_invalid_test
